meta:
  title: '@:home.title ! #JoinMobiliZon'
  description: '@:home.intro.title'
nav:
  langChange: Change the language
  lang: Language
  translate: Translate
menu:
  instances: Instances (to come)
  support: Support
  about: About
link:
  forumPT: https://framacolibri.org/c/mobilizon/fr-francais
  wArticle: https://en.wikipedia.org/wiki/Framasoft
  finance: https://soutenir.framasoft.org/en/
home:
  title: Let’s get back control of our events
  intro:
    tags:
      - gather
      - organize
      - mobilize
    title: Gather, organize and mobilize yourselves with a convivial, ethical, and emancipating tool
    support: Support us
  what:
    text1: Mobilizon is a tool designed to create platforms for <b>managing communities and events</b>.
      Its purpose is to help as many people as possible to free themselves from Facebook groups and events,
      from Meetup, etc.
    text2: The MobiliZon software is under a Free licence, so anyone can host a MobiliZon server,
      called <b>an instance</b>. These instances may federate with each other, so any person with an
      account on <i>"ExampleMeet"</i> will be able to register to an event created on <i>"SpecimenEvent"</i>.
  quote:
    text: We won’t change the world from Facebook. The tool we dream of,
     surveillance capitalism corporations won’t develop it, as they couldn’t profit from it.
     <br> This is an opportunity to build something better, by taking another approach.
    blog:
      text: Read the Framasoft intention note on the Framablog (in French)
      link: https://framablog.org/2018/12/11/mobilizon-reprendre-le-pouvoir-sur-ce-qui-nous-rassemble
  how-it-works:
    how:
      text:
        convivial:
          icon: group
          title: Convivial and practical
          text: MobiliZon doesn’t try to lock you in its platform to manage your community
            nor to direct your ways.<br>
            On the countrary, its goal is to help you integrate the collaborative tools of your choice,
            and to let you be free to organize your community and gatherings your own way.
        emancipation:
          icon: address-card
          title: Emancipating and respectful
          text: MobiliZon gives you the ability to engage without revealing yourself, to organize without exposing yourself. For instance, with just one account, you will get several identities, used as social masks.
        ethic:
          icon: smile-o
          title: Ethical and decentralized
          text: The Free licence of MobiliZon software is a guaranty of its transparency, its contributive aspect and the openness of its governance. Having several MobiliZon instances will let you choose where you want to create your account, so you can decide to whom you will entrust your data.
  timeline:
    image-alt: MobiliZon drawing in Contributopia
    events:
      campaign:
        date: October 2018
        text: Framasoft's donation campaign
        color: current
      study:
        date: Winter 2018
        text: UX studies and user-based design
        color: current
      code:
        date: Spring 2019
        text: Prototyping
        color: mobilizon
      crowdfunding:
        date: May 2019
        text: MobiliZon crowdfunding
        color: mobilizon
      dev:
        date: Summer 2019
        text: Development
        color: mobilizon
      beta:
        date: Fall 2019
        text: Beta release
        color: mobilizon
  donation:
    title: MobiliZon is supported by Framasoft, a non-profit funded by your donations
    texts:
      - <b><span class="frama">Frama</span><span class="soft">soft</span></b> (that's us!)
        is a French non-profit created in 2004, which is now dedicated to
        <b>popular education</b> to freedoms in the digital world. Our small association
        (less than 40 members, less than 10 employees) is known to have achieved
        the <a href="https://degooglisons-internet.org/">De-Google-Ify the Internet</a> project,
        that now offers 34 ethical and alternative online services.
      - Since autumn 2017, we have completed <a href="https://joinpeertube.org/en/">PeerTube</a>,
        a free and federated alternative to centralizing video tubes. After a successful crowdfunding,
        PeerTube is now enjoying growing user base. It is based on this experience
        that we introduce you the MobiliZon project.
      - Labelled as of public interest in France, our non-profit is more than 90% funded by <b>your donations</b>,
        that grant us our independance. Once more, we call for your generosity
        to help us make MobiliZon a reality, while maintaining and pursuing all our actions.

    button:
      read:
        text: Learn more<br>about Framasoft and its actions
        link: https://framasoft.org/en/
      support:
        text: Support<br>our non-profit, Framasoft
        link: https://soutenir.framasoft.org/en/
contact:
  title: Contact us!
  source: Source
  forum:
    link: https://framacolibri.org/c/mobilizon/
footer:
  credits: '© <a href="https://framasoft.org/en/">Framasoft</a> 2018 <a href="https://framagit.org/framasoft/joinmobilizon/joinmobilizon">AGPL-3.0</a> | '
  photo: ' <a href="https://pixabay.com/fr/action-collaborer-collaboration-2277292/">Picture</a> by rawpixel on Pixabay | '
  picture: ' Illustration by <a href="https://davidrevoy.com">David Revoy</a> for <a href="https://contributopia.org/">Contributopia</a>.'